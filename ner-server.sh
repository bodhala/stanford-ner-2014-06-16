#!/bin/bash

CP=stanford-ner.jar 

cd $( dirname $0 )

java -mx1000m -cp classes edu.stanford.nlp.ie.NERServer -loadClassifier classifiers/english.all.3class.distsim.crf.ser.gz -port 1234 # -outputFomat inlineXML

